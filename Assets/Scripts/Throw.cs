﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure; // Required in C#


public class Throw : MonoBehaviour 
{
//	public GameObject thrower;
	//public GameObject throwed;
	private Vector2 hitGround;
	private Vector3 posAtThrow; 
	private int animCount = 50;
	HingeManagement[] hingeManagement = new HingeManagement[4];
	private Input[] inputScript = new Input[4];

	GamePadState state;
	GamePadState prevState;


	GameObject[] CloseObjects = new GameObject[10];
	GameObject[] players = new GameObject[4];

	GameObject clone;
	bool isThrowing = false;
	float throwTime = 5f;
	float time;
	GameObject throwed;

	// Use this for initialization
	void Start () 
	{
		hitGround = Vector2.up;
/*		clone = GameObject.CreatePrimitive (PrimitiveType.Cube);
		clone.transform.renderer.material.color = Color.black;*/
		for (int i = 0; i < hingeManagement.Length; i++)
		{
			players[i] = GameObject.Find("player0" + i.ToString());

			if (players[i] != null)
			{
				hingeManagement[i] = players[i].GetComponent<HingeManagement>();
				inputScript[i] = players[i].GetComponent<Input>();
			}
		}

		time = Time.time;

	}
	
	// Update is called once per frame
	void Update () 
	{
		prevState = state;
		state = GamePad.GetState(0);

	

		if(state.Buttons.X == ButtonState.Pressed && prevState.Buttons.X == ButtonState.Pressed)
		{
			if(state.Buttons.A == ButtonState.Pressed && prevState.Buttons.A != ButtonState.Pressed && !isThrowing)
			{
				isThrowing = true;
				Debug.Log("Throwing: " + isThrowing);
				//throwed = hingeManagement.GetDragee();
				int playerNr = int.Parse( this.transform.name[this.transform.name.Length - 1].ToString())-1;


				if (hingeManagement[playerNr].GetDragee() != null)
				{
					playerNr = int.Parse( hingeManagement[playerNr].GetDragee().transform.name[hingeManagement[playerNr].GetDragee().transform.name.Length - 1].ToString())-1;
					throwed = players[playerNr];

				}
/*				//hitGround = Vector3.zero;
				hitGround = this.transform.up + new Vector3(0, 5f, 0);
				posAtThrow = this.transform.position;
				//Debug.Log("Direction: " + hitGround);
				clone.transform.position = this.transform.position;

				for(int i = 0; i < 20; i++)
				{
					float v = i/20;
					float X = (posAtThrow.y * v) + (hitGround.y * (1 - v));
					
					clone.transform.position = new Vector3(0, X, 0);
				}*/


			}
		}

		if (isThrowing)
		{
			Debug.Log("I'm throwing");
			if(throwed != null)
				throwed.transform.position = ThrowIt(throwed);
		}

		if (Time.time - time > throwTime)
		{
			time = Time.time;
			isThrowing = false;
		}
	


		//clone.transform.position = Vector3.Lerp(posAtThrow, hitGround, Time.time);



/*		for(int i = 0; i < animCount; i++)
		{
			float v = i/animCount;
			v = 1 - (1 - v) * (1 - v);
			float x = (posAtThrow.y * v) + ((hitGround.y) * (1 - v));
			//float y = (this.transform.position.y * v) + ((this.transform.position.y + hitGround.y) * (1 - v));

			clone.transform.localPosition += new Vector3(0, x, 0);
		}*/
	}

	Vector3 ThrowIt(GameObject thrown)
	{
		return thrown.transform.localPosition + thrown.transform.rotation.eulerAngles;
	}

	bool FindCloseObjects(GameObject obj)
	{
		float distance = Vector3.Distance(this.transform.position, obj.transform.position);
		float view = 5f;

		return view > distance;

	}

	bool PlayerNear(GameObject player)
	{
		float distance = Vector3.Distance(this.transform.position, player.transform.position);
		float view = 3f;

		return view > distance;
	}


}
