﻿using UnityEngine;
using System.Collections;

public class PlayerAudio : MonoBehaviour {

	public AudioClip strike01,strike02,strike03,strike04,strike05,strike06,strike07,strike08,getHit,hitSound,missSound,Splat;
	AudioSource source;
	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void playerDeath()
	{
		source.clip = Splat;
		source.Play ();
	}

	public void GotHit()
	{
		source.clip = getHit;
		source.Play ();
	}

	public void leftPunch() 
	{
		int rNumber;
		rNumber = Random.Range (1, 8);
		switch (rNumber) 
		{
		case 1:
			source.clip = strike01;
			source.Play();
			break;
		case 2:
			source.clip = strike02;
			source.Play();
			break;
		case 3:
			source.clip = strike03;
			source.Play();
			break;
		case 4:
			source.clip = strike04;
			source.Play();
			break;
		case 5:
			source.clip = strike05;
			source.Play();
			break;
		case 6:
			source.clip = strike06;
			source.Play();
			break;
		case 7:
			source.clip = strike07;
			source.Play();
			break;
		case 8:
			source.clip = strike08;
			source.Play();
			break;
		}


	}
	public void rightPunch()
	{
		int rNumber;
		rNumber = Random.Range (1, 8);
		switch (rNumber) 
		{
		case 1:
			source.clip = strike01;
			source.Play();
			break;
		case 2:
			source.clip = strike02;
			source.Play();
			break;
		case 3:
			source.clip = strike03;
			source.Play();
			break;
		case 4:
			source.clip = strike04;
			source.Play();
			break;
		case 5:
			source.clip = strike05;
			source.Play();
			break;
		case 6:
			source.clip = strike06;
			source.Play();
			break;
		case 7:
			source.clip = strike07;
			source.Play();
			break;
		case 8:
			source.clip = strike08;
			source.Play();
			break;
		}
	}
}
