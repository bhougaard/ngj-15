﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure; // Required in C#

public class Input : MonoBehaviour {

    int playerNr;

	PlayerMovement playerMovement; 
	GameObject gameManager;
	GameManager gamemanager;

    bool playerIndexSet = false;
    PlayerIndex playerIndex;
    GamePadState state;
    GamePadState prevState;
    public bool XButtonPressed;
    bool movementAllowed;
    
    
    private Vector2 forwardDirection;
    

    void Start() 
    {
        playerNr = int.Parse( this.transform.name[this.transform.name.Length - 1].ToString())-1;
		playerMovement = this.gameObject.GetComponent<PlayerMovement>();

		gameManager = GameObject.Find("GameManager");
		gamemanager = gameManager.GetComponent<GameManager>();
    }
	void Update () 
    {

        forwardDirection = gameObject.transform.up;

        int collisionLayer = 12;
        int collisionMask = 1 << collisionLayer;
        if(Physics2D.Raycast (gameObject.transform.position, forwardDirection, 0.5f, collisionMask))
        {
            movementAllowed = false;
            playerMovement.speed = 0f;
        }
        else
        {
            movementAllowed = true;
            playerMovement.speed = 5f;
        }

        if (!playerIndexSet || !prevState.IsConnected)
        {
            PlayerIndex testPlayerIndex = (PlayerIndex)playerNr;
            GamePadState testState = GamePad.GetState(testPlayerIndex);
            if (testState.IsConnected)
            {
                Debug.Log(string.Format("GamePad found {0}", testPlayerIndex));
                playerIndex = testPlayerIndex;
                playerIndexSet = true;
            }
          
        }
        prevState = state;
        state = GamePad.GetState(playerIndex);

        if (prevState.Buttons.A == ButtonState.Released && state.Buttons.A == ButtonState.Pressed) 
            Debug.Log(string.Format("Player {0} pressed A!", playerNr + 1));
        if (prevState.Buttons.B == ButtonState.Released && state.Buttons.B == ButtonState.Pressed) 
            Debug.Log(string.Format("Player {0} pressed B!", playerNr + 1));
        if (prevState.Buttons.X == ButtonState.Pressed && state.Buttons.X == ButtonState.Pressed)
        {
            XButtonPressed = true;
            Debug.Log(string.Format("Player {0} pressed X!", playerNr + 1));
        }
        else if(prevState.Buttons.X == ButtonState.Pressed && state.Buttons.X == ButtonState.Released) {XButtonPressed = false; }
        if (prevState.Buttons.Y == ButtonState.Released && state.Buttons.Y == ButtonState.Pressed) 
            Debug.Log(string.Format("Player {0} pressed Y!", playerNr + 1));

        if (prevState.Buttons.LeftShoulder == ButtonState.Released && state.Buttons.LeftShoulder == ButtonState.Pressed)
            this.gameObject.SendMessage ("leftPunch");//Debug.Log(string.Format("Player {0} pressed LB!", playerNr + 1));
        if (prevState.Buttons.RightShoulder == ButtonState.Released && state.Buttons.RightShoulder == ButtonState.Pressed)
            this.gameObject.SendMessage("rightPunch");//Debug.Log(string.Format("Player {0} pressed RB!", playerNr + 1));

		if(state.Buttons.LeftShoulder == ButtonState.Pressed && prevState.Buttons.LeftShoulder == ButtonState.Pressed)
		{
			if(state.Buttons.A == ButtonState.Pressed && prevState.Buttons.A != ButtonState.Pressed)
			{
				//gamemanager.ThrowPlayer(this.gameObject, 2);
			}
		}
        if (gameObject != null && movementAllowed)
        {
            transform.localPosition += playerMovement.Move(state.ThumbSticks.Left.X, state.ThumbSticks.Left.Y, prevState.ThumbSticks.Left.X, prevState.ThumbSticks.Left.Y, this.transform.position);
            transform.eulerAngles = playerMovement.Rotate(/* state.ThumbSticks.Right.X, state.ThumbSticks.Right.Y, */ state.ThumbSticks.Left.X, state.ThumbSticks.Left.Y, this.transform.rotation);
        }
        else
        {
            transform.eulerAngles = playerMovement.Rotate(state.ThumbSticks.Right.X, state.ThumbSticks.Right.Y, this.transform.rotation);
        }
        
		if(state.Buttons.Back == ButtonState.Pressed && prevState.Buttons.Back == ButtonState.Released)
		{
			Application.LoadLevel(Application.loadedLevelName);//Restart the game
		}
        
	}
}
