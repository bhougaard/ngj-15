﻿using UnityEngine;
using System.Collections;

public class BoxingScript : MonoBehaviour {

    private enum Handstate { In, Out, Retracting, Extending };
    private Handstate handOneState;
    private Handstate handTwoState;

    private GameObject handOneObj;
    private GameObject handTwoObj;
    private GameObject handOneOrigPosObj;
    private GameObject handTwoOrigPosObj;
    private GameObject handOneTarPosObj;
    private GameObject handTwoTarPosObj;

    //private Vector3 handOneTar;
    //private Vector3 handTwoTar;
    //private Vector3 handOneOrigPos;
    //private Vector3 handTwoOrigPos;

    private float punchDistance = 2f;
    private float punchSpeed = 2;
    private float punchDuration = 10f;

    private float outTimeOne;
    private float outTimeTwo;

    private AnimationController animController;

    private Knockout knockOutScript;
    Vector3 oldPos;

    

    ////////////////////////////////
    // Placeholder variables
    ////////////////////////////////

    private string playerName;

    float raycastLength = 2.1f;


    public void leftPunch() 
    {
        handOneState = Handstate.Extending;
    }
    public void rightPunch()
    {
        handTwoState = Handstate.Extending;
    }

    void Awake()
    {
        playerName = gameObject.name;
        handOneState = Handstate.In;
        handTwoState = Handstate.In;
        handOneObj = GameObject.Find("/" + playerName + "/handOne");
        handTwoObj = GameObject.Find("/" + playerName + "/handTwo");
        handOneOrigPosObj = GameObject.Find("/" + playerName + "/handOneOrigPos");
        handTwoOrigPosObj = GameObject.Find("/" + playerName + "/handTwoOrigPos");
        handOneTarPosObj = GameObject.Find("/" + playerName + "/handOneTarPos");
        handTwoTarPosObj = GameObject.Find("/" + playerName + "/handTwoTarPos");
        animController = GetComponentInChildren<AnimationController>();
        knockOutScript = this.gameObject.GetComponent<Knockout>();
    }

	// Use this for initialization
	void Start () {


        //if (handOneObj != null) { handOneOrigPos = handOneObj.transform.position; }
        //if (handTwoObj != null) { handTwoOrigPos = handTwoObj.transform.position; }
        //handOneTar = handOneOrigPos + (Vector3.up * punchDistance);
        //handTwoTar = handTwoOrigPos + (Vector3.up * punchDistance);
	}
	
	// Update is called once per frame
    void Update()
    {
        if (!(animController.bigboyState == AnimationController.animState.hitLeft || animController.bigboyState == AnimationController.animState.hitRight))
        {
            if (oldPos != gameObject.transform.position)
            {
                animController.bigboyState = AnimationController.animState.walk;
            }
            else
                animController.bigboyState = AnimationController.animState.idle;
        }
        if (handOneObj != null) { MoveLeftHand(); }
        if (handOneObj != null) { MoveRightHand(); }

        oldPos = gameObject.transform.position;
    }

    void MoveRightHand()
    {
        float step = punchSpeed * Time.deltaTime;

        if (handTwoState == Handstate.Extending)
        {
            handTwoObj.transform.position = Vector3.MoveTowards(handTwoObj.transform.position, handTwoTarPosObj.transform.position, step);
            animController.bigboyState = AnimationController.animState.hitRight;
        }

        if (handTwoState == Handstate.Retracting)
        {
            handTwoObj.transform.position = Vector3.MoveTowards(handTwoObj.transform.position, handTwoOrigPosObj.transform.position, step);
            if (oldPos == gameObject.transform.position)
                animController.bigboyState = AnimationController.animState.idle;
            else
                animController.bigboyState = AnimationController.animState.walk;
        }

        if (handTwoObj.transform.position == handTwoTarPosObj.transform.position && handTwoState == Handstate.Extending && handOneObj != null)
        {
            handTwoState = Handstate.Out;
            outTimeTwo = Time.time;
        }

        if (handTwoObj.transform.position == handTwoOrigPosObj.transform.position && handTwoState == Handstate.Retracting)
        {
            handTwoState = Handstate.In;
            //if (handTwoObj != null) { handTwoOrigPosObj.transform.position = handTwoObj.transform.position; }
        }


        if (handTwoState == Handstate.Out)
            if (Time.time - outTimeTwo < punchDuration)
                handTwoState = Handstate.Retracting;

		if (handTwoState == Handstate.Extending || handTwoState == Handstate.Out)
		{
            RaycastHit2D rayHit = Physics2D.Raycast(handTwoOrigPosObj.transform.position, (handTwoObj.transform.position - handTwoOrigPosObj.transform.position) * raycastLength);
            Debug.DrawRay(handTwoOrigPosObj.transform.position, (handTwoObj.transform.position - handTwoOrigPosObj.transform.position) * raycastLength, Color.red);
            if (rayHit.collider != null && rayHit == true && rayHit.collider.gameObject.tag != this.tag )
				if (rayHit.collider.gameObject.tag == "player01" ||  rayHit.collider.gameObject.tag == "player02" || rayHit.collider.gameObject.tag == "player03" || rayHit.collider.gameObject.tag == "player04" || rayHit.collider.gameObject.tag == "Player")
					rayHit.collider.gameObject.transform.parent.gameObject.SendMessage("GotHit");	
		}
    }

    void MoveLeftHand()
    {
        float step = punchSpeed * Time.deltaTime;

        if (handOneState == Handstate.Extending)
        {
            handOneObj.transform.position = Vector3.MoveTowards(handOneObj.transform.position, handOneTarPosObj.transform.position, step);
            animController.bigboyState = AnimationController.animState.hitLeft;

        }

        if (handOneState == Handstate.Retracting)
        {
            handOneObj.transform.position = Vector3.MoveTowards(handOneObj.transform.position, handOneOrigPosObj.transform.position, step);
            if (oldPos == gameObject.transform.position)
                animController.bigboyState = AnimationController.animState.idle;
            else
                animController.bigboyState = AnimationController.animState.walk;
        }

        if (handOneObj.transform.position == handOneTarPosObj.transform.position && handOneState == Handstate.Extending && handTwoObj != null)//Lars
        {
            handOneState = Handstate.Out;
            outTimeOne = Time.time;
        }


        if (handOneObj.transform.position == handOneOrigPosObj.transform.position && handOneState == Handstate.Retracting)
        {
            handOneState = Handstate.In;
            //if (handOneObj != null) { handOneOrigPosObj = handOneObj.transform.position; }
            //handOneTarPosObj.transform.position = handOneOrigPosObj.transform.position /*+ (Vector3.up * punchDistance*/); 
        }

        if (handOneState == Handstate.Out)
        {
            if (Time.time - outTimeOne < punchDuration)
            {
                handOneState = Handstate.Retracting;
            }
        }

        if (handOneState == Handstate.Extending || handOneState == Handstate.Out)
        {
            RaycastHit2D rayHit = Physics2D.Raycast(handOneOrigPosObj.transform.position, (handOneObj.transform.position - handOneOrigPosObj.transform.position) * raycastLength);
            Debug.DrawRay(handOneOrigPosObj.transform.position, (handOneObj.transform.position - handOneOrigPosObj.transform.position) * raycastLength, Color.green);
            if (rayHit.collider != null && rayHit == true && rayHit.collider.gameObject.tag != this.tag)
                if (rayHit.collider.gameObject.tag == "player01" || rayHit.collider.gameObject.tag == "player02" || rayHit.collider.gameObject.tag == "player03" || rayHit.collider.gameObject.tag == "player04" || rayHit.collider.gameObject.tag == "Player")
                    rayHit.collider.gameObject.transform.parent.gameObject.SendMessage("GotHit");	
        }

    } 

}
