﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CarSpawner : MonoBehaviour 
{


	private float startTime = 0f;
    int spawns = 1;
    bool spawnNorth;
    int temp;
	// Use this for initialization
	void Start () 
	{
		//carObj.SetActive(false);
		startTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () 
	{
        if (Time.time / spawns > startTime + Random.Range(1.5f, 1.6f))
            CreateCar();
	}

	private void CreateCar()
	{
        spawns++;  
        int carNumber = (int)Random.Range(0f, 4.9f);
        GameObject car = (GameObject)Instantiate(
            Resources.Load("Prefabs/Cars/car" + carNumber), //Finds a random car model
            (spawnNorth) ? new Vector3(-2, 15, 0) : new Vector3(7, -10, 0), //places the car at the north or south position
             this.transform.rotation);//Places the care so it drives inside the canvas.

        car.name = "car" + carNumber;

        if (spawnNorth)
            car.transform.Rotate(0f, 0f, 10f);
        else
            car.transform.Rotate(0f, 0f, 190f);
        if (Random.Range(0f, 10f) < 4)
        {
            spawnNorth = !spawnNorth;
        }
	}
}
