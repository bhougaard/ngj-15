﻿using UnityEngine;
using System.Collections;

public class Cars : MonoBehaviour
{
	float speed;
	GameObject[] players = new GameObject[4];
	public GameObject bloodPool;


    void Start() 
	{
        speed = Random.Range(0.45f, 0.5f);

		for (int i = 0; i < players.Length; i++)
		{
			if(GameObject.Find("player0" + i.ToString()) != null)
				players[i] = GameObject.Find("player0" + i.ToString());

		}

    }

	void Update()
	{
		for(int i = 0; i < players.Length; i++)
		{
			bool isHit = true;

			if(players[i] != null && HitPlayer(players[i].transform.position))
			{
				if (isHit)
				{
					GameObject clone = Instantiate(bloodPool, players[i].transform.position, players[i].transform.rotation) as GameObject;
					players[i].SendMessage("playerDeath");
					isHit = false;
				}

				players[i].SetActive(false);
			}
		}
	}

	void FixedUpdate()
	{
        if (transform.position.y > 20 || transform.position.y < -16)
            GameObject.Destroy(this.gameObject);
        transform.Translate(Vector3.down * speed, Space.Self);

	}
        

	bool HitPlayer(Vector3 player)
	{
		float dis = Vector3.Distance(this.transform.position, player);
		float view = 2f;

		return view > dis;
	}
}
