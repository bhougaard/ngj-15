﻿using UnityEngine;
using System.Collections;

public class BirdIntel : MonoBehaviour {

  public Animator animator;
  
  GameObject fuglDest;
  Vector2 startPosition;
  public float movementSpeed;
  public float rotationSpeed;
  float startTime;
  float wakeTime;
  
	void Start () {
    fuglDest = GameObject.Find("fuglDest");
    startPosition = this.transform.position; // save initial position.
    movementSpeed = 5.7f*Time.deltaTime;
    rotationSpeed = 1*Time.deltaTime;
    startTime = Time.time;
    wakeTime = startTime + Random.Range(0,200);
    animator = gameObject.GetComponent<Animator>();
	}
	
	void Update () {
	  if (Time.time > wakeTime)
    {
      this.transform.position = Vector2.MoveTowards(this.transform.position, fuglDest.transform.position, 5*Time.deltaTime);
      
      if (birdNear(this.transform.position, fuglDest.transform.position))
      {
       animator.SetBool("idleTime",true); 
      }
    }
	}

  bool birdNear(Vector3 current, Vector3 target) {
    float dist = Vector3.Distance(current,target);
    float view = 4.3f;
    return view > dist;
  }  

}


