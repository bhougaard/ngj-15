﻿using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour 
{
	GameManager gameManager;
	Vector3[] playerPos = new Vector3[4];
	GameObject[] healthBars = new GameObject[4];
	int activePlayers;
	float waitTimer;
	float increase = 10f;
	float timeOut = 50f;
	// Use this for initialization
	void Start () 
	{
		gameManager = this.gameObject.GetComponent<GameManager>();
		activePlayers = gameManager.GetActivePlayers();

		for(int i = 0; i < healthBars.Length; i++)
		{
			healthBars[i] = GameObject.CreatePrimitive(PrimitiveType.Quad);
			healthBars[i].name = "player0" + (i + 1).ToString() + "health";
			healthBars[i].transform.localScale = new Vector3(1f, 0.3f, 1f);
			healthBars[i].transform.renderer.material.color = Color.green;
			healthBars[i].transform.position = new Vector3(30,30,30);


		}

		waitTimer = Time.time;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//activePlayers = gameManager.GetActivePlayers();

		if (gameManager.GetGameState() == 1)
		{
			for(int i = 0; i < playerPos.Length; i++)
			{
				if(gameManager.IsPlayerActive(i))
				{
					playerPos[i] = gameManager.PlayerPos(i);

					if(healthBars[i] != null)
					{
						healthBars[i].transform.position = playerPos[i] + new Vector3(0f, 1f, 0f);
					}

					if (Time.time - waitTimer > timeOut / increase && Mathf.Abs(healthBars[i].transform.localScale.x) < 1)
					{
						waitTimer = Time.time;
						healthBars[i].transform.localScale += new Vector3(0.2f, 0f, -1f);
					}
					else if (Mathf.Abs(healthBars[i].transform.localScale.x) >= 1)
					{
						//healthBars[i].transform.localScale = new Vector3(0f, 0.3f, 1f);
					}
				}
				else
				{
					if(healthBars[i].gameObject != null)
						Destroy(healthBars[i].gameObject);
				}

				foreach(GameObject element in GameManager.knockedOutPlayers)
				{
					//element.GetComponent<Knockout>();
					int playerNr = int.Parse( element.transform.name[element.transform.name.Length - 1].ToString())-1;
					
					SetHealthToZero(playerNr);
					
					if (Mathf.Abs(healthBars[playerNr].transform.localScale.x) >= 1)
					{
						
						//gameManager.RemoveElement(element);
					}
				}
			}



		}

	}

	public void SetHealthToZero(int index)
	{

		healthBars[index].transform.localScale = new Vector3(0f, 0.3f, -1f);

	}

}
