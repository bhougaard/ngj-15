﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HingeManagement : MonoBehaviour {

    public enum DraggingState {dragging, gettingDragged, none};
    public DraggingState playerState;
    private bool isKinematic;
    private Vector2 draggerAnchorPoint;
    private Vector2 drageeAnchorPoint;
    public DistanceJoint2D playerJoint;
    public GameObject dragee;
    public GameObject dragger;
    private Input inputScript;
    private AnimationController animController;

	// Use this for initialization
	void Start () {
        draggerAnchorPoint = GameObject.Find(gameObject.name + "/draggerAnchorPoint").transform.position;
        drageeAnchorPoint = GameObject.Find(gameObject.name + "/drageeAnchorPoint").transform.position;
        playerState = DraggingState.none;
        isKinematic = true;
        playerJoint = gameObject.GetComponent<DistanceJoint2D>();
        playerJoint.enabled = false;
        inputScript = gameObject.GetComponent<Input>();
        animController = gameObject.GetComponentInChildren<AnimationController>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (isKinematic) { rigidbody2D.isKinematic = true; }
        else {rigidbody2D.isKinematic = false;}
        
        if(inputScript.XButtonPressed && playerState == DraggingState.none)
        {
            foreach(GameObject element in GameManager.knockedOutPlayers)
            {
                if(PlayerNear(element))
                {
                    dragee = element;
                    InitiateDragging();
                    element.SendMessage("InitiateGettingDragged", gameObject);

                }
            }
        }
        else if(!inputScript.XButtonPressed && playerState == DraggingState.dragging)
        {
            StopDragging();
            dragee.SendMessage("StopGettingDragged", true);
        }
	}

    void InitiateDragging()
    {
        animController.dragAnimationsEnabled = true;
        isKinematic = true;
        playerState = DraggingState.dragging;
        playerJoint.enabled = true;
        playerJoint.connectedBody = dragee.rigidbody2D;
        playerJoint.anchor = draggerAnchorPoint;
    }

    void StopDragging()
    {
        animController.dragAnimationsEnabled = false;
        playerState = DraggingState.none;
        playerJoint.enabled = false;
    }

    void InitiateGettingDragged(GameObject dragger)
    {
        animController.bigboyState = AnimationController.animState.dragged;
        isKinematic = false;
        playerState = DraggingState.gettingDragged;
        this.dragger = dragger;
        dragger.GetComponent<DistanceJoint2D>().connectedAnchor = drageeAnchorPoint;
    }

    void StopGettingDragged(bool isKinematic)
    {
        animController.bigboyState = AnimationController.animState.damage;
        this.isKinematic = isKinematic;
        playerState = DraggingState.none;
    }

    bool PlayerNear(GameObject playerObjs)
    {
        float distance = Vector3.Distance(playerObjs.transform.position, gameObject.transform.position);
        float searchRange = 2f;
        return searchRange > distance;
    }

	public GameObject GetDragee()
	{
		return dragee;
	}
}
