﻿using UnityEngine;
using System.Collections;

public class AnimationController : MonoBehaviour {

    public Animator animator;

    public enum animState { idle, walk, damage, hitLeft, hitRight, awake, draggingIdle, draggingWalk, throwState, draggedTrans, dragged, beingThrown};
    public animState bigboyState;
    public bool dragAnimationsEnabled = false;
   

	// Use this for initialization
	void Start () {
	    animator = gameObject.GetComponent<Animator>();
        if (gameObject.transform.parent.gameObject.name == "player01")
        {
            animator.runtimeAnimatorController = (RuntimeAnimatorController) Resources.Load("Animations/playerBlueController");
        }
        else if (gameObject.transform.parent.gameObject.name == "player02")
        {
            animator.runtimeAnimatorController = (RuntimeAnimatorController)Resources.Load("Animations/playerRedController");
        }
        else if (gameObject.transform.parent.gameObject.name == "player03")
        {
            animator.runtimeAnimatorController = (RuntimeAnimatorController)Resources.Load("Animations/playerYellowController");
        }
        else if (gameObject.transform.parent.gameObject.name == "player04")
        {
            animator.runtimeAnimatorController = (RuntimeAnimatorController)Resources.Load("Animations/playerPinkController");
        }

	}
	
	// Update is called once per frame
	void Update () {
	    if(animator)
        {
            if(bigboyState == animState.idle && dragAnimationsEnabled == false)
            {
                animator.SetBool("animIdle", true);
                animator.SetBool("animWalk", false);
                animator.SetBool("animHit", false);
                animator.SetBool("animDmg", false);
                animator.SetBool("animAwake", false);
                animator.SetBool("animDraggingIdle", false);
                animator.SetBool("animDraggingWalk", false);
                animator.SetBool("animThrow", false);
                animator.SetBool("animDraggedTrans", false);
                animator.SetBool("animDragged", false);
                animator.SetBool("animBeingThrown", false);
            }
            else if(bigboyState == animState.idle && dragAnimationsEnabled)
            {
                animator.SetBool("animIdle", false);
                animator.SetBool("animWalk", false);
                animator.SetBool("animHit", false);
                animator.SetBool("animDmg", false);
                animator.SetBool("animAwake", false);
                animator.SetBool("animDraggingIdle", true);
                animator.SetBool("animDraggingWalk", false);
                animator.SetBool("animThrow", false);
                animator.SetBool("animDraggedTrans", false);
                animator.SetBool("animDragged", false);
                animator.SetBool("animBeingThrown", false);
            }
            if (bigboyState == animState.walk && dragAnimationsEnabled == false)
            {
                animator.SetBool("animIdle", false);
                animator.SetBool("animWalk", true);
                animator.SetBool("animHit", false);
                animator.SetBool("animDmg", false);
                animator.SetBool("animAwake", false);
                animator.SetBool("animDraggingIdle", false);
                animator.SetBool("animDraggingWalk", false);
                animator.SetBool("animThrow", false);
                animator.SetBool("animDraggedTrans", false);
                animator.SetBool("animDragged", false);
                animator.SetBool("animBeingThrown", false);
            }
            else if(bigboyState == animState.walk && dragAnimationsEnabled)
            {
                animator.SetBool("animIdle", false);
                animator.SetBool("animWalk", false);
                animator.SetBool("animHit", false);
                animator.SetBool("animDmg", false);
                animator.SetBool("animAwake", false);
                animator.SetBool("animDraggingIdle", false);
                animator.SetBool("animDraggingWalk", true);
                animator.SetBool("animThrow", false);
                animator.SetBool("animDraggedTrans", false);
                animator.SetBool("animDragged", false);
                animator.SetBool("animBeingThrown", false);
            }
            if (bigboyState == animState.damage )
            {
                animator.SetBool("animIdle", false);
                animator.SetBool("animWalk", false);
                animator.SetBool("animHit", false);
                animator.SetBool("animDmg", true);
                animator.SetBool("animAwake", false);
                animator.SetBool("animDraggingIdle", false);
                animator.SetBool("animDraggingWalk", false);
                animator.SetBool("animThrow", false);
                animator.SetBool("animDraggedTrans", false);
                animator.SetBool("animDragged", false);
                animator.SetBool("animBeingThrown", false);
            }
            if (bigboyState == animState.hitLeft)
            {
                gameObject.transform.localScale = new Vector3(1, 1, 1);
                animator.SetBool("animIdle", false);
                animator.SetBool("animWalk", false);
                animator.SetBool("animHit", true);
                animator.SetBool("animDmg", false);
                animator.SetBool("animAwake", false);
                animator.SetBool("animDraggingIdle", false);
                animator.SetBool("animDraggingWalk", false);
                animator.SetBool("animThrow", false);
                animator.SetBool("animDraggedTrans", false);
                animator.SetBool("animDragged", false);
                animator.SetBool("animBeingThrown", false);
            }
            if(bigboyState == animState.hitRight)
            {
                gameObject.transform.localScale = new Vector3(-1,1,1);
                animator.SetBool("animIdle", false);
                animator.SetBool("animWalk", false);
                animator.SetBool("animHit", true);
                animator.SetBool("animDmg", false);
                animator.SetBool("animAwake", false);
                animator.SetBool("animDraggingIdle", false);
                animator.SetBool("animDraggingWalk", false);
                animator.SetBool("animThrow", false);
                animator.SetBool("animDraggedTrans", false);
                animator.SetBool("animDragged", false);
                animator.SetBool("animBeingThrown", false);
            }
            if(bigboyState == animState.awake)
            {
                animator.SetBool("animIdle", false);
                animator.SetBool("animWalk", false);
                animator.SetBool("animHit", false);
                animator.SetBool("animDmg", false);
                animator.SetBool("animAwake", true);
                animator.SetBool("animDraggingIdle", false);
                animator.SetBool("animDraggingWalk", false);
                animator.SetBool("animThrow", false);
                animator.SetBool("animDraggedTrans", false);
                animator.SetBool("animDragged", false);
                animator.SetBool("animBeingThrown", false);
            }
            if(bigboyState == animState.throwState)
            {
                animator.SetBool("animIdle", false);
                animator.SetBool("animWalk", false);
                animator.SetBool("animHit", false);
                animator.SetBool("animDmg", false);
                animator.SetBool("animAwake", false);
                animator.SetBool("animDraggingIdle", false);
                animator.SetBool("animDraggingWalk", false);
                animator.SetBool("animThrow", true);
                animator.SetBool("animDraggedTrans", false);
                animator.SetBool("animDragged", false);
                animator.SetBool("animBeingThrown", false);
            }
            if(bigboyState == animState.dragged)
            {
                animator.SetBool("animIdle", false);
                animator.SetBool("animWalk", false);
                animator.SetBool("animHit", false);
                animator.SetBool("animDmg", false);
                animator.SetBool("animAwake", false);
                animator.SetBool("animDraggingIdle", false);
                animator.SetBool("animDraggingWalk", false);
                animator.SetBool("animThrow", false);
                animator.SetBool("animDraggedTrans", false);
                animator.SetBool("animDragged", true);
                animator.SetBool("animBeingThrown", false);
            }
            if(bigboyState == animState.beingThrown)
            {
                animator.SetBool("animIdle", false);
                animator.SetBool("animWalk", false);
                animator.SetBool("animHit", false);
                animator.SetBool("animDmg", false);
                animator.SetBool("animAwake", false);
                animator.SetBool("animDraggingIdle", false);
                animator.SetBool("animDraggingWalk", false);
                animator.SetBool("animThrow", false);
                animator.SetBool("animDraggedTrans", false);
                animator.SetBool("animDragged", false);
                animator.SetBool("animBeingThrown", true);
            }
        }

	}
}