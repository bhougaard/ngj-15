﻿using UnityEngine;
using System.Collections;

public class Knockout : MonoBehaviour
{

    public bool knockedOut;
    public int hitsTaken;
    private float knockDownAnimDur;
    private float knockDownTimer;
    private float timeOut;
    private AnimationController playerAnimControl;
    private Input playerInput;
    private BoxingScript playerBoxing;
    public bool gotHit;
    public bool disableControls;
    private HingeManagement jointScript;

	bool addedToList = false;

    // Use this for initialization
    void Start()
    {
        disableControls = false;
        gotHit = false;
        hitsTaken = 0;
        knockDownAnimDur = 0.8f;
        timeOut = 5f;
        playerAnimControl = GetComponentInChildren<AnimationController>();
        jointScript = GetComponent<HingeManagement>();

        playerInput = gameObject.GetComponent<Input>();
        playerBoxing = gameObject.GetComponent<BoxingScript>();

    // Update is called once per frame
    }
    void Update()
    {

        if (knockedOut == true)
        {
            if (jointScript.playerState == HingeManagement.DraggingState.gettingDragged)
            {
                playerAnimControl.animator.speed = 1f;
            }
            else if ((Time.time - knockDownTimer > knockDownAnimDur) && knockedOut)
            {
                playerAnimControl.animator.speed = 0f;
            }
             
            
            if (Time.time - knockDownTimer > timeOut)
            {
                playerAnimControl.animator.speed = 1f;
                playerAnimControl.bigboyState = AnimationController.animState.awake;
                knockedOut = false;
                if(jointScript.playerState == HingeManagement.DraggingState.gettingDragged)
                {
                    jointScript.SendMessage("StopGettingDragged", true);
                    jointScript.dragger.SendMessage("StopDragging");
                }
            }
            hitsTaken = 0;
        }
        else if (hitsTaken > 5 && knockedOut == false)
        {
            playerAnimControl.bigboyState = AnimationController.animState.damage;
            knockDownTimer = Time.time;
            knockedOut = true;
        }

        if (knockedOut)
        {
            disableControls = true;
			if(!addedToList)
            {
                GameManager.knockedOutPlayers.Add(gameObject);
				//int playerNr = int.Parse( this.transform.name[this.transform.name.Length - 1].ToString())-1;
				addedToList = true;
            }
				
        }
        else
        {
            addedToList = false;
            disableControls = false;
            GameManager.knockedOutPlayers.Remove(gameObject);
        }
            
        if(disableControls)
        {
            playerBoxing.enabled = false;
            playerInput.enabled = false;
            foreach (BoxCollider2D element in gameObject.GetComponentsInChildren<BoxCollider2D>())
            {
                element.enabled = false;
            }
        }
        else
        {
            playerBoxing.enabled = true;
            playerInput.enabled = true;
            foreach (BoxCollider2D element in gameObject.GetComponentsInChildren<BoxCollider2D>())
            {
                element.enabled = true;
            }
        }

    }
    void GotHit()
    {
        hitsTaken++;
    }
}
