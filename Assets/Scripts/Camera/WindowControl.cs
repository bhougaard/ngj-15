﻿using UnityEngine;
using System.Collections;

public class WindowControl : MonoBehaviour 
{
	public GameObject objOne;
	public GameObject objTwo;


	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		float distance = Vector2.Distance(objOne.transform.position, objTwo.transform.position);

		Camera.main.orthographicSize = distance + 10f;

		this.transform.position = new Vector3(objOne.transform.position.x - objTwo.transform.position.x ,objOne.transform.position.y - objTwo.transform.position.y, 0); 
	}
}
