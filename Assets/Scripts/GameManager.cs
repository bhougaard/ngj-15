﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using XInputDotNetPure; // Required in C#


public class GameManager : MonoBehaviour 
{

	int activePlayers;
	GameObject[] Players = new GameObject[4]; 
	bool[] isPlayerActive = new bool[4]{false,false,false,false};
	bool[] isKnockedOut = new bool[4]{false, false,false,false};
	int[] score = new int[4]{0,0,0,0};
	int[] deaths = new int[4]{0,0,0,0};
	float time;
    float endTime;

	string[] colors = new string[4]
	{
		"5E7186",//Rød
		"69584C",//Blå
		"877A79",//pink
		"4AE7C"//Gul
	};

	float ResetTime;

	//Fake GUI
	GameObject[] LockIn = new GameObject[4];

	int gameState = 0;
	bool endConditionMet = false;

	//xInput variables
	bool playerIndexSet = false;
	PlayerIndex playerIndex;
	GamePadState[] state = new GamePadState[4];
	GamePadState[] prevState = new GamePadState[4];
    public static List<GameObject> knockedOutPlayers = new List<GameObject>();

	// Use this for initialization
	void Start () 
	{
		activePlayers = 0;

		if (!playerIndexSet /*|| !prevState.IsConnected*/)
		{
			for (int i = 0; i < 4; ++i)
			{
				PlayerIndex testPlayerIndex = (PlayerIndex)i;
				GamePadState testState = GamePad.GetState(testPlayerIndex);
				if (testState.IsConnected)
				{
					Debug.Log(string.Format("GamePad found {0}", testPlayerIndex));
					playerIndex = testPlayerIndex;
					playerIndexSet = true;
					activePlayers++;
					//Debug.Log("Active players: " + activePlayers);

				}
			}
		}

	for (int i = 0; i < 4; i++)
		{
			Players[i] = GameObject.Find("player0" + (i + 1).ToString());

			if (Players[i] != null)
				Players[i].SetActive(false);

			LockIn[i] = GameObject.CreatePrimitive(PrimitiveType.Quad);
			LockIn[i].transform.position = new Vector3(2 * i - 3.5f, 0, - 2);
			//string col = "player" + i.ToString();
			LockIn[i].transform.renderer.material.color = Color.black; 
		}

		ResetTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () 
	{
		/*if(Time.time - ResetTime > 10)
		{
			RemoveElements();
		}*/

		time = Time.realtimeSinceStartup;

		switch(gameState)
		{
		case 0:
			//Lobby round - Players join in
			LobbyState();
			break;
		case 1:
			//Active gameplay
			PlayState();
			break;
		case 2:
			//Winner screen
			EndState();
			break;
		case 3:
			//Pause game
			PauseGameState();
			break;
		default:
			//gameState = 1;
			break;
		}
	}

	private void LobbyState()
	{
		//Player select
		for(int i = 0; i < 4; i++)
		{
			prevState[i] = state[i];
			state[i] = GamePad.GetState((PlayerIndex)i);

			if (state[i].Buttons.Start == ButtonState.Pressed && prevState[i].Buttons.Start != ButtonState.Pressed)
			{
				if (!isPlayerActive[i])
				{
					isPlayerActive[i] = true;
					LockIn[i].transform.renderer.material.color = HexToColor(colors[i]); 
				}
				else
				{
					isPlayerActive[i] = false;
					LockIn[i].transform.renderer.material.color = Color.black; 
				}
			}

			if(state[i].Buttons.A == ButtonState.Pressed && prevState[i].Buttons.A != ButtonState.Pressed)
			{
              
				SetupPlayState();
			}
		}

		//When the players have joined and pressed ready change to next state
	/*	if (true)
			SetupPlayState();*/
	}

	private void SetupPlayState()
	{
		//Setting up the game state
		for (int i = 0; i < 4; i++)
		{
			if (isPlayerActive[i])
			{
				Players[i] = (GameObject)Instantiate(Resources.Load("Prefabs/player"));
				Players[i].transform.position = new Vector3(2 * i - 3.5f, 0, 0);
				Players[i].name = "player0" + (i + 1);
				Players[i].tag = "player0" + (i + 1);
				GameObject.Find (Players[i].name + "/handOne").tag = "player0" + (i + 1);
				GameObject.Find (Players[i].name + "/handTwo").tag = "player0" + (i + 1);
				GameObject.Find (Players[i].name + "/torsoCollider").tag = "player0" + (i + 1);

				Players[i].SetActive(true);
			}

			Destroy(LockIn[i]);
		}

		gameState = 1;
	}

	private void PlayState()
	{
		//Active game state


		if(endConditionMet)
		{
			//End game
            endTime = Time.time;
			gameState = 2;
		}
	}

	private void EndState()
	{

        //Show win screen
        if (Time.time > endTime + 9)
            RestartGame();
	}

	private void PauseGameState()
	{
		//Pause the game
	}

	private void RestartGame()
	{
        Application.LoadLevel(Application.loadedLevelName);//Restart the game
	}

	public int GetGameState()
	{
		return gameState;
	}

	//Returns the number of players playing the game
	public int GetActivePlayers()
	{
		return activePlayers;
	}

	//Tells if a specific player is currently active
	public bool IsPlayerActive(int index)
	{
		return Players[index] != null; 
	}

	//Returns the score of a player
	public int GetScore(int index)
	{
		return score[index];
	}

	//Returns the current time the game has been running
	public float GetTime()
	{
		return time; 
	}

	//Sets the score of a player by some amount
	public void SetScore(int index, int amount)
	{
		score[index] += amount;
	}

	//Sets the death count of a player by some amount
	public void SetDeaths(int index, int amount)
	{
		deaths[index] += amount;
	}


	public void ThrowPlayer(GameObject Thrower, int playerNr)
	{
		Vector3 goal = Thrower.transform.up = Vector3.up * 3;
		Players[playerNr].transform.position = Vector3.Lerp(Thrower.transform.position, goal, (Vector3.Distance(Thrower.transform.position, goal) * Time.deltaTime));
	}

	public void DisablePlayer(int playerNr)
	{
		Players[playerNr].SetActive(false);
	}

	public Vector3 PlayerPos(int playerNr)
	{
		return Players[playerNr].transform.position;
	}

	public void RemoveElements()
	{
		knockedOutPlayers.Clear();
	}

	public void SetKnockedOut(int index, bool state)
	{
		isKnockedOut[index] = state;
	}

	public bool GetKnockedOut(int index)
	{
		return isKnockedOut[index];
	}
    void OnGUI()
    {
        if (gameState == 2)
        {
            int winner = 0;
            for (int i = 0; i < 3; i++)
                if (deaths[i] == 0)
                    winner = i;
            GUI.Label(new Rect(Screen.width / 2, Screen.height / 2 -10, 200, 20), "Player " + score.Max() + " scored highest!");
            GUI.Label(new Rect(Screen.width / 2, Screen.height / 2 +10, 200, 20), "Player " + winner + " scored highest!");
        }
    }

	Color HexToColor(string hex)
	{
		byte r = byte.Parse(hex.Substring(0,2), System.Globalization.NumberStyles.HexNumber);
		byte g = byte.Parse(hex.Substring(2,2), System.Globalization.NumberStyles.HexNumber);
		byte b = byte.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);
		return new Color32(r,g,b, 255);
	}
}
