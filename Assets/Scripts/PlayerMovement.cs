﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour 
{
	public float speed = 5.0f;


	public Vector3 Move(float x, float y, float pX, float pY, Vector2 player)
	{
		Vector3 step = Vector3.zero;
		int steps = 100;  

		if ((x != 0 || y != 0) && (pX == 0 && pY == 0))
		{
			for (int i = 0; i < steps; i++)
			{
				float v = i / steps;
				//v = Smoothstep(v);
				//v = Mathf.Asin(v * Mathf.PI / 2);
				//step = new Vector3((player.x * v) + (x * speed * Time.deltaTime * (1 - v) ), (player.y * v) + (y * speed * Time.deltaTime * (1 - v) ),0);
				step = new Vector3(CubicEaseIn(Time.deltaTime, x, x + v, 10f), CubicEaseIn(Time.deltaTime, y, y + v, 10f),0);

			}
			//Debug.Log("Start");
		}
		else if ((x != 0 || y != 0) && (pX != 0 || pY != 0))
		{
			step = new Vector3(x * speed * Time.deltaTime, y * speed * Time.deltaTime, 0.0f);
			//Debug.Log("Ongoing");
		}
		else if ((x == 0 && y == 0) && (pX != 0 || pY != 0))
		{
			for (int i = 0; i < steps; i++)
			{
				float v = i / steps;
				//v = Smoothstep(v);
				//v = Mathf.Asin(v * Mathf.PI / 2);
				//step = new Vector3((player.x * v) + (x * speed * Time.deltaTime * (1 - v) ), (player.y * v) + (y * speed * Time.deltaTime * (1 - v) ),0);
				step = new Vector3(CubicEaseOut(Time.deltaTime, x, x + v, 10f), CubicEaseIn(Time.deltaTime, y, y + v, 10f),0);

			}
			//Debug.Log("Stopping");
		}
		//Debug.Log(step);
		return step;
	}

	public Vector3 Rotate(/*float x, float y,*/ float lX, float lY, Quaternion player)
	{
		Vector3 rotation = player.eulerAngles;

/*		if (x != 0 || y != 0)
		{
			rotation = new Vector3(0, 0, Mathf.Atan2(y, x) * Mathf.Rad2Deg - 90);
		}
		else if (x == 0 && y == 0 &&*/ 

		if( lX == 0 && lY == 0)
		{
			rotation = player.eulerAngles;
		}
		else
		{
			rotation = new Vector3(0, 0, -Mathf.Atan2(lX, lY) * Mathf.Rad2Deg);
		}

		//rotation = new Vector3(0, 0, -Mathf.Atan2(lX, lY) * Mathf.Rad2Deg);


		return rotation;
	}

	float Smoothstep(float x)
	{
		return x * x * (3 - 2 * x);
	}

	/// <summary>
	/// Easing equation function for a cubic (t^3) easing in: 
	/// accelerating from zero velocity.
	/// </summary>
	/// <param name="t">Current time in seconds.</param>
	/// <param name="b">Starting value.</param>
	/// <param name="c">Final value.</param>
	/// <param name="d">Duration of animation.</param>
	/// <returns>The correct value.</returns>
	public static float CubicEaseIn( float t, float b, float c, float d )
	{
		return c * ( t /= d ) * t * t + b;
	}

	/// <summary>
	/// Easing equation function for a cubic (t^3) easing out: 
	/// decelerating from zero velocity.
	/// </summary>
	/// <param name="t">Current time in seconds.</param>
	/// <param name="b">Starting value.</param>
	/// <param name="c">Final value.</param>
	/// <param name="d">Duration of animation.</param>
	/// <returns>The correct value.</returns>
	public static float CubicEaseOut( float t, float b, float c, float d )
	{
		return c * ( ( t = t / d - 1 ) * t * t + 1 ) + b;
	}


}
